#include <vector>
#include "Heap.h"

using std::vector;

void HeapSort(vector<int>& a) {
    THeap myheap;
    myheap.BuildHeapFromArray(a);
    for (int i = 0; i < a.size(); ++i) {
        a[i] = myheap.RemoveTop();
    }
}

int main() {
    int a;
    std::cin >> a;
    vector<int> array;
    for (int i = 0; i < a; ++i) {
        array.push_back(rand() % 10);
    }
    for (int i = 0; i < a; ++i) {
        std::cout << array[i]<< " ";
    }
    std::cout << "\n";
    HeapSort(array);
    for (int i = 0; i < a; ++i) {
        std::cout << array[i] << " ";
    }
    return 0;
}