//
// Created by jenya on 13.11.2020.
//

#ifndef BINARYTREE_TREE_H
#define BINARYTREE_TREE_H

#include <iostream>
#include <vector>
#include <map>


using std::vector;
using std::cin;
using std::string;
using std::map;
using std::cout;



struct TreeNode {
    map <string, int> Vertex;
    TreeNode* Right;
    TreeNode* Left;
    TreeNode* Parent;
};


class TTree {
private:
    TreeNode* NewNode(int X, TreeNode* parent = nullptr) {
        TreeNode* node = new TreeNode;
        node->Vertex["Value"] = X;
        node->Left = nullptr;
        node->Right = nullptr;
        node->Parent = parent;
        return node;
    }

    void SetNode(int X, TreeNode* node) {
        if (X > node->Vertex["Value"]) {
            if (node->Right == nullptr) {
                node->Right = NewNode(X, node);
                return;
            }
            SetNode(X, node->Right);
        } else if (X < node->Vertex["Value"]) {
            if (node->Left == nullptr) {
                node->Left = NewNode(X, node);
                return;
            }
            SetNode(X, node->Left);
        }
    }

    bool GetNode(int X, TreeNode* node) {
        if (X == node->Vertex["Value"]) {
            Node = node;
            return true;
        } else if (X < node->Vertex["Value"]) {
            if (node->Left == nullptr) {
                return false;
            }
            GetNode(X, node->Left);
        } else if (X > node->Vertex["Value"]) {
            if (node->Right == nullptr) {
                return false;
            }
            GetNode(X, node->Right);
        }
    }

    void PrintNode(int X , TreeNode* node) {
        int l = 0;
        int r = 0;
        TreeNode* leftnode;
        TreeNode* rightnode;
        if (X == node->Vertex["Value"]) {
            if (node->Right != nullptr) {
                rightnode = node->Right;
                r = rightnode->Vertex["Value"];
            }
            if (node->Left != nullptr) {
                leftnode = node->Left;
                l = leftnode->Vertex["Value"];
            }
        }

    }

    void SetParentsLink(TreeNode* node) {
        if (Node->Parent->Right == Node) {
            Node->Parent->Right = node;
        }
        else {
            Node->Parent->Left = node;
        }
    }

    vector<int> ListMap;
    TreeNode* Head = nullptr;
    TreeNode* Node;
public:

    TTree(){};

    void AddNode(int X) {
        if (Head == nullptr) {
            Head = NewNode(X);
        } else  if(!GetNode( X, Head)) {
            SetNode(X, Head);
        }
    }

    void DeleteNode(int X) {
        TreeNode* Kid;
        if (GetNode(X, Head)) {
            PrintNode(Node->Parent->Vertex["Value"], Node->Parent);
            PrintNode(Node->Vertex["Value"] , Node);
            if (Node->Right == nullptr) {
                SetParentsLink(Node->Left);
            } else {
                Kid = Node->Right;
                if (Kid->Left == nullptr) {
                    Kid->Left = Node->Left;
                    SetParentsLink(Node->Right);
                } else {
                    while (Kid->Left != nullptr) {
                        Kid = Kid->Left;
                    }
                    Kid->Parent->Left = Kid->Right;
                    Kid->Right = Node->Right;
                    Kid->Left = Node->Left;
                    SetParentsLink(Kid);
                }
            }
            PrintNode(Node->Parent->Vertex["Value"], Node->Parent);
        }
    }



    bool findNode(int X) {
        if (GetNode(X, Head )) {
            PrintNode(X , Node);
            return true;
        }
        else {
            return false;
        }
    }


    void infixRound(TreeNode* node) {
        if (node == nullptr) {
            return;
        }
        infixRound(node->Left);
        ListMap.push_back(node->Vertex["Value"]);
        infixRound(node->Right);
    }

    void printList() {
        ListMap.clear();
        infixRound(Head);
        for(int i = 0; i < ListMap.size(); ++i) {
            cout << ListMap[i]<< ", ";
        }
        cout << "\n";
    }


};


#endif //BINARYTREE_TREE_H
